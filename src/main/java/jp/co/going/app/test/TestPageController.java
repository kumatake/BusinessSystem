package jp.co.going.app.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kumatake on 2017/07/09.
 */
@RestController
public class TestPageController {

    @RequestMapping("/")
    public String index() {
        return "Hello Going!";
    }

}
